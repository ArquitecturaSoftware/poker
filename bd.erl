-module(bd).
-export([arrancarBd/0,cerrarBd/0]).

arrancarBd()->  BD = spawn(?Node,fun()-> loop([]) end),
                    global:register_name(?Node,BD),
                    io:format("Arrancando base de datos...~n",[]).
cerrarBd()->
    global:whereis_name(?Node) ! stop.

existe(_,[])->
    notExist;
existe(K,[{K,_,0}|_])->
    inactive;
existe(K,[{K,_,1}|_])->
    active;
existe(K,[{_,_,_}|T])->
    existe(K,T).

loop(L)->
    receive
        {?Servidor,estaRegistradoJ,{Login,Pass,Jugador,From}} ->
            AUX = existe(Login,L),
			case AUX of
				notExist->
					Msg = "Login o contraseña erronea.",
					L1= L,
					Bolean = false;
				active	->
					Msg = "Usuario ya logueado.",
					L1=L,
					Bolean = false;
				inactive->
					Msg = "Usuario logueado correctamente.",
					Bolean = true,
					{Login, Pass, _} = lists:keyfind(Login, 1, L),
					L1 = lists:keyreplace(Login, 1, L , {Login, Pass, 1})
			end,
            From ! {{Login,Pass,Jugador},Bolean,Msg},
            loop(L1);
        {?Servidor,addJugador,{Login,Pass,Jugador,From}}->
            AUX = existe(Login,L),
            case AUX of 
                notExist->
                    L1 = [{Login,Pass,0} | L],
                    Msg = "Usuario registrado satisfactoriamente.";
                _Else ->
                    L1 = L,
                    Msg = "Este usuario ya existe, eliga otro."
            end,
            From ! {registro,Jugador,Msg},
            loop(L1);
        stop->
            global:unregister_name(?Node)
    end.
