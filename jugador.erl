-module(jugador).
-export([startPlayers/0, loguearse/2,verSalas/1,entrarSala/3,registrarse/2,modificarJugador/3, subir/2, igualar/1, abandonarRonda/1, abandonarPartida/1, misCartas/1, verCartasMesa/1]).

startPlayers()	->net_adm:ping(?Servidor).

loguearse(Login,Pass)-> global:whereis_name(?Servidor) ! {login,Login,Pass,self()},
						receive
							{login,false,Msg}	->	 	io:format("~p~n",[Msg]);
							{login,true,Msg}	->		io:format("~p~n",[Msg]),spawn(?Node,fun()->jugador(Login,Pass,true,false,false,[],[],300,0,0, false) end)
							
						end.

							

modificarJugador(Login,Pass, JugadorId)->
    JugadorId ! {nuevo,Login,Pass}.
    
registrarse(Login,Pass)->   
    global:whereis_name(?Servidor) ! {registro,Login,Pass,self()},
    receive
		 {registro,Msg}->  io:format("~p~n",[Msg])
    end.
        
 
misCartas(JugadorId)->
	JugadorId ! {ver,misCartas}.
	
verCartasMesa(JugadorId)->
	JugadorId ! {ver,cartas,mesa}.

verSalas(JugadorId)->
    JugadorId! versalas.

entrarSala(Sala,Tipo, JugadorId)->
    JugadorId ! {entrarsala,Sala,Tipo}.
    
subir(JugadorId, Cantidad)->
	JugadorId ! {subir, Cantidad}.

igualar(JugadorId)->
	JugadorId ! {igualarApuesta}.
	
abandonarRonda(JugadorId)->
	JugadorId ! {abandonarRonda}.

abandonarPartida(JugadorId)->
	JugadorId ! {abandonarPartida}.


jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno)->
receive

	{ganadorRondaDefault, Login, Bet,Msg}	->	io:format("Has ganado porque  ~p ~n",[Msg]),
											jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas+Bet, Dealer, ApuestaMinima, MiTurno);
	 
	{ganadorRondaDefault, Log, _,Msg}		->	io:format("Ha ganado ~p porque ~p ~n",[Log,Msg]),
											jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
										
	{ganadorRonda, Login, _, Bet}	-> 	io:format("Eres el ganador jugador ~p. Has ganado ~p fichas.~n",[Login, Bet]),
										jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas+Bet, Dealer, ApuestaMinima, MiTurno);
	
	{ganadorRonda, Log, Jugada, _}	->	io:format("Lo sentimos jugador ~p. Has perdido la ronda.~n El ganador ha sido ~p con ~p.~n",[Login, Log, Jugada]),
										jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);

	
	{finRonda, enseñaCartas} -> Dealer ! {mostrarCartas,CartasMano, Login},jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
	{ver,misCartas}-> 
		case Jugando of
			false	->	io:format("No tienes cartas porque no estás jugando~n"),jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
			true	->	io:format("Mis cartas son: ~p ~n",[CartasMano]), jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno)
		end;
	{ver,cartas,mesa}->  
		case Jugando of
			false	->	io:format("No hay cartas porque no estás jugando~n"),jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
			true	->	io:format("Las cartas en la mesa son: ~p ~n",[CartasMesa]), jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno)
		end;
	
    {nuevo,L,P}->
        jugador(L,P,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);             
  
    versalas->
        case Logueado of 
                false->
                    io:format("No puede ver la sala si no esta logueado.~n"), jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
                true->
                    global:whereis_name(?Servidor) ! {mostrarSalas, self()}
            end,
            jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
            
    {mostrarSalas, L1, L2}->
            io:format("Lista de salas de Chill: ~p ~n",[L1]),
            io:format("Lista de salas de Torneo: ~p ~n",[L2]),
            jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
            
    {entrarsala,Sala,Tipo}->
            case Logueado of 
                false->
                    io:format("No puede ver la sala si no esta logueado.~n");
                true->	case Jugando of
							false 	->	global:whereis_name(?Servidor) ! {unirse,Sala,Tipo,self()};
							true	->	io:format("No puedes entrar en otra sala si ya estas en una~n")
						end
            end,
            jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
            
    {empezarMano,Msg,false}->
        io:format("~p~n",[Msg]),
        jugador(Login,Pass,Logueado,true,false, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
        
    {empezarMano,Msg,true}->
		io:format("~p~n",[Msg]),
        jugador(Login,Pass,Logueado,EnSala,true, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
    
    {subir, Cantidad}->
		case Jugando of
			false	->	io:format("No puedes subir si no estas jugando.~n"),
						jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, MiTurno);
			
			true	->
				case MiTurno of
					false	->	io:format("No puedes subir si no es tu turno.~n"),
								jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, MiTurno);
					true	->	if 
									Fichas-Cantidad<0		->	io:format("No tienes suficientes fichas como para subir. Fichas Restantes ~p",[Fichas]),jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
									Cantidad>ApuestaMinima	->	Dealer ! {subir, Cantidad}, jugador(Login,Pass,Logueado,EnSala,true, CartasMesa, CartasMano, Fichas-Cantidad, Dealer, ApuestaMinima, false);
											true			->	io:format("No llega a la apuesta minima de ~p",[ApuestaMinima]),jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno)
								end
				end
					
		end;
		
	 {igualarApuesta}->
		case Jugando of
			false	->	io:format("No puedes igualar si no estas jugando.~n"),
						jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, MiTurno);
			
			true	->	
				case MiTurno of
					false	->	io:format("No puedes igualar si no es tu turno.~n"),
								jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, MiTurno);
					true	->	if 
									Fichas-ApuestaMinima<0	->	io:format("No tienes suficientes fichas como para igualar. Fichas Restantes ~p",[Fichas]),jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
									true	->	Dealer ! igualar,jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, false)
								end
				end
		end;
		
	{abandonarRonda}->
		case Jugando of
			false	->	io:format("No puedes abandonar si no estas jugando.~n"),
						jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, MiTurno);
			
			true	->	
			case MiTurno of
					false	->	io:format("No puedes abandonar si no es tu turno.~n"),
								jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, MiTurno);
								
					true	->	Dealer ! {abandonarRonda}, 
								jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, false)
			end
		end;
	{abandonarPartida}->
		case Jugando of
			false	->	io:format("No puedes abandonar si no estas jugando.~n"),
						jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, MiTurno);
			
			true	->	
			case MiTurno of
					false	->	io:format("No puedes abandonar si no es tu turno.~n"),
								jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, MiTurno);
								
					true	->	Dealer ! {abandonarPartida}, 
								jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, ApuestaMinima, false)
			end
		end;
		
	{mano,Mano,From}-> 	io:format("Soy el jugador ~p. Mis cartas son ~p y mi dealer es ~p ~n",[Login,Mano,From]),
						jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, Mano, Fichas, From, ApuestaMinima, MiTurno);
	
	{bet,NewApuestaMinima} ->
		io:format("TURNO DE JUGADOR ~p PARA APOSTAR: MINIMO ~p ~n",[Login, NewApuestaMinima]),
		jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas-ApuestaMinima, Dealer, NewApuestaMinima, true);


    {tableCard, CartaMesa} ->
		io:format("Nueva Carta en la mesa ~p ~n",[CartaMesa]),
		jugador(Login,Pass,Logueado,EnSala,Jugando, [CartaMesa|CartasMesa], CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno);
	
	 {abandonaMesa,Msg}	->
		io:format("~p~n",[Msg]),
		jugador(Login,Pass,Logueado,false,false, [], [], Fichas, 0, 0, false);
		
							
    {Msg}->
        io:format("~p~n",[Msg]),
       jugador(Login,Pass,Logueado,EnSala,Jugando, CartasMesa, CartasMano, Fichas, Dealer, ApuestaMinima, MiTurno)
end.
