-module(ranking).
-export([start/0, loop/1, show_ranking/0, rank_user/2, stop/0]).

start()->
    Pid = spawn(?MODULE, loop, [#{}]),
    net_adm:ping(?Servidor),
    global:register_name(?Node,Pid),
    io:format("Arrancando ranking ~n").

show_ranking() ->
    global:whereis_name(?Node) ! show_ranking.

rank_user(Login, Node) ->
    global:whereis_name(?Node) ! {rank, Login, Node}.

stop() ->
    global:whereis_name(?Node) ! stop.

% Rankings es un map
loop(Rankings)->
    receive
        { rank,Login, Whom }->
            Server = global:whereis_name(?Servidor),
            case Whom of
                Server ->
                    Updated_rankings = update_ranking(Rankings, Login),
                    loop(Updated_rankings);
                _ -> 
                    io:format("Unauthorized attempt to rank a user"),
                    loop(Rankings)
            end;
        show_ranking ->
            show_ranking(Rankings),
            loop(Rankings);
        stop ->
            erlang:display("Stopping"),
            unregister(repositorio),
            ok
    end.

update_ranking(Rankings, Login) ->
    Fun = fun(V) -> V + 1 end,
    maps:update_with(Login,Fun,1,Rankings).


show_ranking(Rankings) ->
    List = maps:to_list(Rankings),
    Sorted_list = lists:reverse(lists:keysort(2, List)),
    print_ranking(Sorted_list, 1).


print_ranking([], _) ->
    ok;
print_ranking([{Login, Wins} | T], N) ->
    io:format("Posicion: ~p -> Usuario: ~p con ~p rondas ganadas ~n", [N, Login, Wins]),
    print_ranking(T, N+1).
