%%%-------------------------------------------------------------------
%%% @author Gabriel Sánchez <g.sanchezc@udc.es>
%%% @copyright (C) 2017, Gabriel Sánchezc
%%% @doc Baraja de Poker
%%% @end
%%%-------------------------------------------------------------------
-module(baraja).
-export([startBaraja/0, barajar/1, sacarCarta/1, rankingCarta/1, hand_rank/1, sort_hands/1, winners/1]).

startBaraja() ->
  [{Numero, Palo} || Numero <- ["A", 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K"],
    Palo <- ["Picas", "Corazones", "Treboles", "Diamantes"]].

barajar(List) 		-> barajar(List, []).

barajar([H],Acc) 	-> [H|Acc];

barajar(List, Acc) 	->
	{Leading, [H | T]} = lists:split(rand:uniform(length(List)) - 1, List),
	barajar(Leading ++ T, [H | Acc]).
	
	

sacarCarta([H|T])	->	[H,T].


rankingCarta(Mano)	->	NuevaMano= convertidorAInt(Mano,[]),
						lists:sort(fun erlang:'>'/2, NuevaMano).


convertidorAInt([], Resultado)					-> Resultado;
convertidorAInt([{"J", _}|T], Resultado)		-> convertidorAInt(T, [11|Resultado]);
convertidorAInt([{"Q", _}|T], Resultado)		-> convertidorAInt(T, [12|Resultado]);
convertidorAInt([{"K", _}|T], Resultado)		-> convertidorAInt(T, [13|Resultado]);
convertidorAInt([{"A", _}|T], Resultado)		-> convertidorAInt(T, [14|Resultado]);
convertidorAInt([{Numero, _}|T], Resultado)	-> convertidorAInt(T, [Numero|Resultado]).


hand_rank(Hand) ->
    CardRanks = rankingCarta(Hand),
    HandRanks = [R || F <- poker_hands(),
                      begin R = F(CardRanks, Hand), R /= undefined end],
hd(HandRanks).

poker_hands() ->
    [
        fun straight_flush/2,
        fun four_of_kind/2,
        fun full_house/2,
        fun flush/2,
        fun straight/2,
        fun three_of_kind/2,
        fun two_pair/2,
        fun pair/2,
        fun high_card/2
].

straight_flush(Ranks, Hand) ->
    case {straight(Ranks,Hand), flush(Ranks,Hand)} of
        {[4,R], [5|_]} -> [8, R];
        _              -> undefined
    end.


%------------------------------------%

%Comprobamos si hay poker

four_of_kind([H,H,H,H,_,_,_], _) -> [7, H];
four_of_kind([_,H,H,H,H,_,_], _) -> [7, H];
four_of_kind([_,_,H,H,H,H,_], _) -> [7, H];
four_of_kind([_,_,_,H,H,H,H], _) -> [7, H];


four_of_kind(_,_) -> undefined.



%------------------------------------%



%Comprobamos si hay full.
%%Con cartas indiferentes a la derecha
full_house([H,H,H,L,L,_,_], _) -> [6, H, L];
full_house([H,H,L,L,L,_,_], _) -> [6, L, H];

%%Con cartas indiferentes a la izquierda
full_house([_,_,H,H,L,L,L], _) -> [6, H, L];
full_house([_,_,H,H,H,L,L], _) -> [6, H, L];

%%Con cartas indiferentes por el medio
full_house([H,H,H,_,L,L,_], _) -> [6, H, L];
full_house([H,H,H,_,_,L,L], _) -> [6, H, L];
full_house([H,H,_,L,L,L,_], _) -> [6, H, L];
full_house([H,H,_,_,L,L,L], _) -> [6, H, L];

%%Con cartas indiferentes en los bordes
full_house([_,H,H,L,L,L,_], _) -> [6, H, L];
full_house([_,H,H,H,L,L,_], _) -> [6, H, L];

full_house(_,_) -> undefined.



%------------------------------------%



%Comprobamos si hay color.

%Dejamos primera posicion quieta.
flush(Ranks, [{_,S}, {_,S}, {_,S}, {_,S}, {_,S}, {_,_}, {_,_}]) -> [5 | Ranks];


flush(Ranks, [{_,S}, {_,_}, {_,_}, {_,S}, {_,S}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,_}, {_,S}, {_,_}, {_,S}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,_}, {_,S}, {_,S}, {_,_}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,_}, {_,S}, {_,S}, {_,S}, {_,_}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,_}, {_,S}, {_,S}, {_,S}, {_,S}, {_,_}]) -> [5 | Ranks];

%Dejamos primera y segunda posicion quieta
flush(Ranks, [{_,S}, {_,S}, {_,_}, {_,_}, {_,S}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,S}, {_,_}, {_,S}, {_,_}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,S}, {_,_}, {_,S}, {_,S}, {_,_}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,S}, {_,_}, {_,S}, {_,S}, {_,S}, {_,_}]) -> [5 | Ranks];

%Dejamos 1ª,2ª y 3ª posicion quieta
flush(Ranks, [{_,S}, {_,S}, {_,S}, {_,_}, {_,_}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,S}, {_,S}, {_,_}, {_,S}, {_,_}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,S}, {_,S}, {_,_}, {_,S}, {_,S}, {_,_}]) -> [5 | Ranks];

%Dejamos 1ª, 2ª, 3ª y 4ª posicion quieta

flush(Ranks, [{_,S}, {_,S}, {_,S}, {_,S}, {_,_}, {_,_}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,S}, {_,S}, {_,S}, {_,S}, {_,_}, {_,S}, {_,_}]) -> [5 | Ranks];


flush(Ranks, [{_,_}, {_,_}, {_,S}, {_,S}, {_,S}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,_}, {_,S}, {_,_}, {_,S}, {_,S}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,_}, {_,S}, {_,S}, {_,_}, {_,S}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,_}, {_,S}, {_,S}, {_,S}, {_,_}, {_,S}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,_}, {_,S}, {_,S}, {_,S}, {_,S}, {_,_}, {_,S}]) -> [5 | Ranks];
flush(Ranks, [{_,_}, {_,S}, {_,S}, {_,S}, {_,S}, {_,S}, {_,_}]) -> [5 | Ranks];


flush(_,_) -> undefined.


%------------------------------------%

%Comprobamos si hay escalera

%Dejamos quieto R1
    
straight([R1, R2, R3, R4, R5,_,_], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];


straight([R1,_,_, R2, R3, R4, R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];

straight([R1,_, R2,_, R3, R4, R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];
    
straight([R1,_, R2, R3,_, R4, R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];
    
straight([R1,_, R2, R3, R4,_, R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];
   
straight([R1,_, R2, R3, R4, R5,_], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];
    
    
%Dejamos quieto R1 y R2.
    
straight([R1, R2,_,_, R3, R4, R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];
        
straight([R1, R2,_, R3,_, R4, R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];
   
straight([R1, R2,_,R3,R4,_,R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];
   
straight([R1,R2,_,R3,R4,R5,_], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];



    
%Dejamos quieto R1, R2 y R3.

straight([R1,R2,R3,_,_,R4,R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];

straight([R1,R2,R3,_,R4,_,R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];

straight([R1,R2,R3,_,R4,R5,_], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];

%Dejamos quieto R1, R2, R3 y R4.
    
straight([R1,R2,R3,R4,_,_,R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];
    
straight([R1, R2,R3,R4,_,R5,_], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1;
    R1==14, R2==5, R3==4, R4==3, R5==2-> [4, R1];


%Movemos R1 una posicion.    
straight([_,R1,_,R2,R3,R4,R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1 -> [4, R1];
    
straight([_,R1,R2,_,R3,R4,R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1 -> [4, R1];

straight([_,R1,R2,R3,_,R4,R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1 -> [4, R1];

straight([_,R1,R2,R3,R4,_,R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1 -> [4, R1];

straight([_,R1,R2,R3,R4,R5,_], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1 -> [4, R1];
    
    
%Movemos R1 dos posiciones.
    
straight([_,_, R1, R2, R3, R4, R5], _)
    when R1 == R2+1, R2 == R3+1, R3 == R4+1, R4 == R5+1 -> [4, R1];

straight(_,_) -> undefined.


%------------------------------------%


%Comprobamos si hay trios.

three_of_kind([R,R,R,_,_,_,_] = Ranks, _) -> [3, R | Ranks];
three_of_kind([_,R,R,R,_,_,_] = Ranks, _) -> [3, R | Ranks];
three_of_kind([_,_,R,R,R,_,_] = Ranks, _) -> [3, R | Ranks];
three_of_kind([_,_,_,R,R,R,_] = Ranks, _) -> [3, R | Ranks];
three_of_kind([_,_,_,_,R,R,R] = Ranks, _) -> [3, R | Ranks];

three_of_kind(_,_) -> undefined.


%------------------------------------%



%Comprobamos si hay dobles parejas.
two_pair([H,H,L,L,_,_,_] = Ranks, _) -> [2, H, L | Ranks];
two_pair([H,H,_,L,L,_,_] = Ranks, _) -> [2, H, L | Ranks];
two_pair([H,H,_,_,L,L,_] = Ranks, _) -> [2, H, L | Ranks];
two_pair([H,H,_,_,_,L,L] = Ranks, _) -> [2, H, L | Ranks];

two_pair([_,H,H,L,L,_,_] = Ranks, _) -> [2, H, L | Ranks];
two_pair([_,H,H,_,L,L,_] = Ranks, _) -> [2, H, L | Ranks];
two_pair([_,H,H,_,_,L,L] = Ranks, _) -> [2, H, L | Ranks];

two_pair([_,_,H,H,L,L,_] = Ranks, _) -> [2, H, L | Ranks];
two_pair([_,_,H,H,_,L,L] = Ranks, _) -> [2, H, L | Ranks];

two_pair([_,_,_,H,H,L,L] = Ranks, _) -> [2, H, L | Ranks];

two_pair(_,_) -> undefined.


%%Comprobamos si hay parejas.
pair([R,R,_,_,_,_,_] = Ranks, _) -> [1, R | Ranks];
pair([_,R,R,_,_,_,_] = Ranks, _) -> [1, R | Ranks];
pair([_,_,R,R,_,_,_] = Ranks, _) -> [1, R | Ranks];
pair([_,_,_,R,R,_,_] = Ranks, _) -> [1, R | Ranks];
pair([_,_,_,_,R,R,_] = Ranks, _) -> [1, R | Ranks];
pair([_,_,_,_,_,R,R] = Ranks, _) -> [1, R | Ranks];

pair(_,_) -> undefined.


%------------------------------------%


high_card(Ranks, _) -> [0 | Ranks].

sort_hands(Hands) ->
    lists:sort(fun(H1, H2) -> hand_rank(H2) =< hand_rank(H1) end, Hands).

winners(Hands) ->
    SortedHands = sort_hands(Hands),
    HighestRank = hand_rank(hd(SortedHands)),
    [H || H <- SortedHands, hand_rank(H) == HighestRank].
