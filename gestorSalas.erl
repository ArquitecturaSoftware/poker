%%%-------------------------------------------------------------------
%%% @author Gabriel Sánchez <g.sanchezc@udc.es>
%%% @copyright (C) 2017, Gabriel Sánchezc
%%% @doc Gestor de Salas de Poker
%%% @end
%%%-------------------------------------------------------------------
-module(gestorSalas).
-export([startGestorSalas/2,gestorDeSalas/5,playLoop/7,salaChill/4,salaTorneo/1,cerrarPoker/0]).


startGestorSalas(NumSalasChill,NumSalasTorneo)	->	GS = spawn(?Node, gestorSalas,gestorDeSalas,[[],0,NumSalasChill, [],0]),
														net_adm:ping(?BD), %se conecta al nodo donde esta la bd
														global:register_name(?Node,GS),
														createSalas(NumSalasChill, "chill"),
														createSalas(NumSalasTorneo, "torneo").

createSalas(0, _)		->	ok;					
createSalas(1, Opt)		->	startSalas(Opt);					
createSalas(NumSalas, Opt)	->	startSalas(Opt), createSalas(NumSalas-1, Opt).

startSalas("chill")	->	SalaChill = spawn(?Node, gestorSalas, salaChill, [[],false,0,[]]),
						global:whereis_name(?Node) ! {newSalaChill, SalaChill};
									
startSalas("torneo")	->	SalaTorneo = spawn(?Node, gestorSalas, salaTorneo, [[]]),
						global:whereis_name(?Node) ! {newSalaTorneo, SalaTorneo}.	

cerrarPoker()->
	global:whereis_name(?Node) ! stop.
gestorDeSalas(SalaChillList, SalasChillLlenas,SalasChillLibres, SalaTorneoList, SalasTorneoLlenas)	->
		
		receive
			{registro,Login,Pass,From}->
				global:whereis_name(?BD) ! {?Node,addJugador,{Login,Pass,From,self()}},
				?MODULE:gestorDeSalas(SalaChillList, SalasChillLlenas, SalasChillLibres, SalaTorneoList, SalasTorneoLlenas);
				
			{registro,Jugador,Msg}->
				Jugador ! {registro,Msg},
				?MODULE:gestorDeSalas(SalaChillList, SalasChillLlenas, SalasChillLibres, SalaTorneoList, SalasTorneoLlenas);
				
			{login,Login,Pass,From}->
				global:whereis_name(?BD) ! {?Node,estaRegistradoJ,{Login,Pass,From,self()}},
				?MODULE:gestorDeSalas(SalaChillList, SalasChillLlenas, SalasChillLibres, SalaTorneoList, SalasTorneoLlenas);
				
			{{_,_,From},S,M}->
				From ! {login,S,M},
				
				?MODULE:gestorDeSalas(SalaChillList, SalasChillLlenas, SalasChillLibres, SalaTorneoList, SalasTorneoLlenas);

			{newSalaTorneo, SalaTorneo}->
				io:format("Se añade una nueva sala de torneo ~n"),
				?MODULE:gestorDeSalas(SalaChillList, SalasChillLlenas, SalasChillLibres, [{SalaTorneo,0}|SalaTorneoList], SalasTorneoLlenas);
				
			{newSalaChill, SalaChill} ->
				io:format("Se añade una nueva sala de chill ~n"),
				?MODULE:gestorDeSalas([{SalaChill,0}|SalaChillList], SalasChillLlenas, SalasChillLibres+1, SalaTorneoList, SalasTorneoLlenas);
				
			{mostrarSalas, From}-> 	
				From ! {mostrarSalas, SalaChillList, SalaTorneoList},
				?MODULE:gestorDeSalas(SalaChillList, SalasChillLlenas,SalasChillLibres, SalaTorneoList, SalasTorneoLlenas);
			
			{actualizar, "chill", Sala, Jugador}->  
				{Sala, Jugadores} = lists:keyfind(Sala, 1, SalaChillList),
				ActualizadaSalaChillList = lists:keyreplace(Sala, 1, SalaChillList , {Sala, Jugadores-1}),
				Msg = "Has abandonado la mesa satisfactoriamente.",
				Jugador ! {abandonaMesa,Msg},
				if 
					(Jugadores-1)==0	->
											if
												length(SalaChillList)=<2	->	?MODULE:gestorDeSalas(ActualizadaSalaChillList, SalasChillLlenas,SalasChillLibres+1, SalaTorneoList, SalasTorneoLlenas);
												(SalasChillLibres+1)>=round((length(ActualizadaSalaChillList)*40)/100)	-> 	io:format("Se borra una salaChill~n"),
																															Sala ! stop,
																															?MODULE:gestorDeSalas(ActualizadaSalaChillList, SalasChillLlenas,SalasChillLibres-1, SalaTorneoList, SalasTorneoLlenas);
															true			->?MODULE:gestorDeSalas(ActualizadaSalaChillList, SalasChillLlenas,SalasChillLibres+1, SalaTorneoList, SalasTorneoLlenas)
											end;
							true		->	?MODULE:gestorDeSalas(ActualizadaSalaChillList, SalasChillLlenas,SalasChillLibres, SalaTorneoList, SalasTorneoLlenas)
				end;
																															
				
			{actualizar, "chill", Sala}->  
				{Sala, Jugadores} = lists:keyfind(Sala, 1, SalaChillList),
				ActualizadaSalaChillList = lists:keyreplace(Sala, 1, SalaChillList , {Sala, Jugadores+1}),
				if
					(Jugadores+1)==2 	->	
											if
												(SalasChillLlenas+1)>=round((length(ActualizadaSalaChillList)*70)/100)	-> 	io:format("Se crea una nueva salaChill~n"),
																															SalaChill = spawn(?Node, gestorSalas, salaChill, [[],false,0,[]]),
																															global:whereis_name(?Node) ! {newSalaChill, SalaChill};
																true													-> 	ok
											end,
											?MODULE:gestorDeSalas(ActualizadaSalaChillList, SalasChillLlenas+1,SalasChillLibres,  SalaTorneoList, SalasTorneoLlenas);
											
						true			->	?MODULE:gestorDeSalas(ActualizadaSalaChillList, SalasChillLlenas,SalasChillLibres, SalaTorneoList, SalasTorneoLlenas)
				end;
				
			{actualizar, "torneo", Sala}->  									
				{Sala, Jugadores} = lists:keyfind(Sala, 1, SalaTorneoList),
				ActualizadaSalaTorneoList = lists:keyreplace(Sala, 1, SalaTorneoList , {Sala, Jugadores+1}),
				if
					(Jugadores+1)==8 	->	
											if
												(SalasTorneoLlenas+1)>round((length(ActualizadaSalaTorneoList)*70)/100)	-> 	SalaTorneo = spawn(?Node, gestorSalas, salaTorneo, [[],false]),
																															global:whereis_name(?Node) ! {newSalaTorneo, SalaTorneo};
																true													-> 	ok
											end,
											?MODULE:gestorDeSalas(SalaChillList, SalasChillLlenas ,SalasChillLibres, ActualizadaSalaTorneoList, SalasTorneoLlenas+1);
											
						true			->	?MODULE:gestorDeSalas(SalaChillList, SalasChillLlenas,SalasChillLibres, ActualizadaSalaTorneoList, SalasTorneoLlenas)
				end;
				
				
			{unirse,Sala,"chill",Jugador}->
				{Sala, Jugadores} = lists:keyfind(Sala, 1, SalaChillList),
				if Jugadores <9 ->
					Jugador ! {"Ha entrado en la sala.",true},
					Sala ! {unirse,Jugador,"chill",self()};
				true->
					Jugador ! {"La sala esta llena.",false}				
				end,
				?MODULE:gestorDeSalas(SalaChillList, SalasChillLlenas,SalasChillLibres, SalaTorneoList, SalasTorneoLlenas);			
			stop->
				io:format("Cerrando servicio..."),
				global:unregister_name(?Node)
		end.

%%REVISAR
salaChill(JugadoresList,JugandoMano, Dealer, ListaEspera)	->
		
			receive
				stop		-> ok;
					
				{enEspera}	->
								if 
									length(ListaEspera)>0	->
										Dealer ! {enEspera, ListaEspera},
										salaChill(JugadoresList,JugandoMano, Dealer, []);
									true					->
										Dealer ! {noEnEspera},
										salaChill(JugadoresList,JugandoMano, Dealer, ListaEspera)
								end;
				
				{abandonarPartida, Jugador}->	io:format("Eliminamos al jugador ~p ~n",[Jugador]),
												NuevaJugadoresList = lists:delete(Jugador,JugadoresList),
												global:whereis_name(?Node) ! {actualizar,"chill",self(),Jugador},
												salaChill(NuevaJugadoresList,JugandoMano, Dealer, ListaEspera);
								
						
				
				{unirse, Jugador,Tipo,From}-> 
												io:format("La lista de espera es ~p ~n",[ListaEspera]),
												L1 = [Jugador | JugadoresList],
												From ! {actualizar,Tipo,self()},
												if 
													length(L1)==2->
														Msg="Puedes comenzar a jugar.",
														enviarInfo(L1,Msg,true),
														NewDealer = callDealer(L1, self()),
														NuevaListaEspera = ListaEspera;
													length(L1)>2->
														NewDealer = Dealer,
														Msg="La partida ya está empezada. Espera a que acabe la ronda.",
														enviarInfo([Jugador],Msg,true),
														NuevaListaEspera = [Jugador|ListaEspera];
													true->
														NewDealer = Dealer,
														Msg="Es necesario un jugador mas.",
														NuevaListaEspera = ListaEspera,
														enviarInfo(L1,Msg,false)
												end,
												?MODULE:salaChill(L1,JugandoMano, NewDealer ,NuevaListaEspera )				
			end.
			

sendCards(B, []) ->
	B;
sendCards(B,[H|T]) -> 
	[Carta1, Mazo] = baraja:sacarCarta(B),
	[Carta2, Mazo1] = baraja:sacarCarta(Mazo),
	Mano = [Carta1, Carta2],
	H ! {mano,Mano,self()},
	sendCards(Mazo1,T).

sendCard(_,[]) ->
ok;
sendCard(Carta,[H|T])->
	H ! {tableCard, Carta},
	sendCard(Carta,T).

sendTableCard(B,Players) ->
	[Carta1, Mazo] = baraja:sacarCarta(B),
	sendCard(Carta1,Players),
	[Carta1,Mazo].


betLoop([],Bet,Listos,Ab,_) ->
	[Bet,Listos,Ab];

betLoop([H|T],BetMin,Listos,Ab,Sala) ->
	H ! {bet, BetMin},
	receive
		{subir,Bet} ->
			betLoop(T++Listos,Bet,[H|[]],Ab,Sala);
		{abandonarPartida}	->
			io:format("Le decimos a la sala ~p que abandono la partida ~p ~n",[Sala,H]),
			Sala ! {abandonarPartida, H},
			betLoop(T,BetMin,Listos,[H|Ab],Sala);
		igualar ->
			betLoop(T,BetMin,[H|Listos],Ab,Sala);
		{abandonarRonda} ->
			betLoop(T,BetMin,Listos,Ab,Sala)
	end.



evaluadorCartas([],_,JugadasConLog, Jugadas)	-> [JugadasConLog,Jugadas];

evaluadorCartas([H|T],CartasMesa, JugadasConLog, Jugadas)	->
	H ! {finRonda, enseñaCartas},
	receive
		{mostrarCartas,Cartas, Login}	->
						Jugada =CartasMesa++Cartas,
						evaluadorCartas(T,CartasMesa,[{Login,Jugada}|JugadasConLog],[Jugada|Jugadas])
	end.

notificarGanadorPorAbandonos([],{_,_},_)->ok;

notificarGanadorPorAbandonos([H|T],{Log,X},Bet)->
	Msg="el resto abandono la ronda",
	H ! {ganadorRondaDefault, Log, Bet,Msg},
	notificarGanadorPorAbandonos(T,{Log,X},Bet).
						

notificarGanador([],{_,_},_)->ok;

notificarGanador([H|T],{Log,Jugada},Bet)->
	H ! {ganadorRonda, Log, Jugada,Bet},
	notificarGanador(T,{Log,Jugada},Bet).
						

salirSala([],NewList)			->	NewList;
salirSala([H|T], List)	->	NewList = lists:delete(H,List),
									salirSala(T, NewList).	

playLoop(PlayersList, PlayersRound,Round,Baraja,TableCards,Bet, Sala) ->
	if length(PlayersList)<2	-> stop;
		true	->
					case Round of
						0 ->
							io:format("Repartiendo Cartas a ~p~n",[PlayersRound]),
							B1 = sendCards(Baraja,PlayersList),
							playLoop(PlayersList,PlayersRound,Round+1,B1,[],0, Sala);
						1 -> 
							%%PREFLOP
							io:format("PREFLOP ROUND ~n"),
							[Rbet,L1,Ab] = betLoop(PlayersRound,0,[],[], Sala),
							if
								length(Ab)>0	->	NewPlayerList = salirSala(Ab,PlayersList);
								true			->	NewPlayerList = PlayersList
							end,
							if 
								length(L1)==1	->	playLoop(NewPlayerList, L1,5,Baraja,TableCards,(Rbet*length(L1))+Bet, Sala);
									true		->	playLoop(NewPlayerList, L1,Round+1,Baraja,TableCards,(Rbet*length(L1))+Bet, Sala)
							end;
						2 -> 
							%%FLOP
							io:format("FLOP ROUND ~n"),
							[Carta1,B1] = sendTableCard(Baraja,PlayersRound),
							[Carta2,B2] = sendTableCard(B1,PlayersRound),
							[Carta3,B3] = sendTableCard(B2,PlayersRound),
							[Rbet,L1,Ab] = betLoop(PlayersList,0,[],[], Sala),
							if
								length(Ab)>0	->	NewPlayerList = salirSala(Ab,PlayersList);
								true			->	NewPlayerList = PlayersList
							end,
							if 
								length(L1)==1	->	playLoop(NewPlayerList, L1,5,B3,[Carta1,Carta2,Carta3],(Rbet*length(L1))+Bet, Sala);
									true		->	playLoop(NewPlayerList, L1,Round+1,B3,[Carta1,Carta2,Carta3],(Rbet*length(L1))+Bet, Sala)
							end;
						3 ->
							%%TURN
							io:format("TURN ROUND ~n"),
							[Carta1,B1] = sendTableCard(Baraja,PlayersRound),
							[Rbet,L1,Ab] = betLoop(PlayersList,0,[],[],Sala),
							if
								length(Ab)>0	->	NewPlayerList = salirSala(Ab,PlayersList);
								true			->	NewPlayerList = PlayersList
							end,
							if 
								length(L1)==1	->	playLoop(NewPlayerList, L1,5,B1,[Carta1|TableCards],(Rbet*length(L1))+Bet, Sala);
									true		->	playLoop(NewPlayerList, L1,Round+1,B1,[Carta1|TableCards],(Rbet*length(L1))+Bet, Sala)
							end;
						4 ->
							%%RIVER
							io:format("RIVER ROUND ~n"),
							[Carta1,B1] = sendTableCard(Baraja,PlayersRound),
							[Rbet,L1,Ab] = betLoop(PlayersList,0,[],[],Sala),
							if
								length(Ab)>0	->	NewPlayerList = salirSala(Ab,PlayersList);
								true			->	NewPlayerList = PlayersList
							end,
							playLoop(NewPlayerList, L1,Round+1,B1,[Carta1|TableCards],(Rbet*length(L1))+Bet, Sala);
						5 ->
							[JugadasConLogin, Jugadas] = evaluadorCartas(PlayersRound, TableCards,[],[]),
							if 
								length(PlayersRound)/=1	->
									[JugadasConLogin, Jugadas] = evaluadorCartas(PlayersRound, TableCards,[],[]),
									JugadaGanadora = baraja:winners(Jugadas),
									{Log,Jugada} = lists:keyfind(lists:flatten(JugadaGanadora),2,JugadasConLogin),
									ranking:rank_user(Log, global:whereis_name(?Node)),
									notificarGanador(PlayersList,{Log,Jugada}, Bet),
									Sala ! {enEspera},
									receive
										 {enEspera, ListaEspera}	->	NewPlayerList=PlayersList++ListaEspera, playLoop(NewPlayerList, NewPlayerList, 0,crearBaraja(),[],0, Sala);
										 {noEnEspera}				->	playLoop(PlayersList,PlayersList, 0, crearBaraja(),[],0, Sala)
									end;
									
								length(PlayersRound) ==1 ->
									notificarGanadorPorAbandonos(PlayersList,hd(JugadasConLogin),Bet),
									{Log,_} = hd(JugadasConLogin),
									ranking:rank_user(Log, global:whereis_name(?Node)),
									Sala ! {enEspera},
									receive
										 {enEspera, ListaEspera}	->	NewPlayerList=PlayersList++ListaEspera, playLoop(NewPlayerList, NewPlayerList, 0,crearBaraja(),[],0, Sala);
										 {noEnEspera}				->	playLoop(PlayersList,PlayersList, 0, crearBaraja(),[],0, Sala)
									end
							end;
						_Else -> playLoop(PlayersList,PlayersRound, Round,Baraja,TableCards,Bet, Sala) 
					end
	end.

			
			
	salaTorneo(JugadoresList)	->
		
			receive
				
				stop						-> ok;
				
				{unirse, Jugador}			-> ?MODULE:salaChill([Jugador|JugadoresList]);
			
				{apuesta, Fichas, Jugador}	->	io:format("~n jugando torneo  ~n"),
												timer:sleep(rand:uniform(10)),
												io:format("~n El jugador ~p ha ganado ~p fichas ~n",[Jugador, Fichas]),
												{gestorDeSalas,'serverSalas@LA-BESTIA'} ! {finPartida, self()},
												?MODULE:salaTorneo(JugadoresList)
			
			end.
			

crearBaraja()	->	B = baraja:startBaraja(),
					baraja:barajar(B).
					
callDealer(PlayersList, Sala) 	->	B1 = crearBaraja(),
									spawn(?Node, gestorSalas,playLoop,[PlayersList,PlayersList,0,B1,[],0, Sala]).

enviarInfo([],_,_)->
	ok;
enviarInfo([H|T],Msg,State)->
	io:format("Se avisa a ~p ~n",[H]),
	H ! {empezarMano,Msg,State},
	enviarInfo(T,Msg,State).

